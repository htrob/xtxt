#!/usr/bin/ruby
usage = "Usage: muxt output_filename filename1 filename2 [filename3, ...]"

if ARGV.size < 3 then 
    STDERR.puts "Error: bad arguments"
    STDERR.puts usage 
    exit 1
end

outfname = ARGV.shift 
outfname=outfname+".xtxt" if outfname == outfname.rpartition(".").last 
   
puts "Muxing: ("+ ARGV.join(", ")+") into: #{outfname}"

streams = []
ARGV.each {|fname|
	lines = File.open(fname).readlines
	begin
		0.upto(lines.size-1) {|ln| lines[ln].unicode_normalize!}
	rescue
		STDERR.puts "Error: Invalid unicode in #{fname}"
		exit 1
	end
	streams << lines
}

NSM = "\xFF\xFF"  # next stream marker
NFM = "\xFF\xFE"  # next frame marker
NCM = "\xFF\xFD"  # next chunk marker

output = ""

stream_sizes = []
streams.each {|astream| 
	stream_sizes << astream.size
}

longest = (stream_sizes.max) - 1

=begin 
require 'pp'
require 'digest/md5'
pp stream_sizes 
pp longest 	
=end

0.upto(longest) {|lineno|
	chunk=""
    #puts lineno
	streams.each {|astream|
        aline = astream[lineno] ? astream[lineno] : ""


        chunk << aline + NSM
        #chunk << aline+"  "  # add two bytes
        #chunk.setbyte(chunk.bytes.size-2, 0xff) # set NSM first byte
        #chunk.setbyte(chunk.bytes.size-1, 0xff) # set NSM second byte
    }
=begin
        hexa=""
        chunk.bytes.each {|b|
            hexa << "%02x".%(b)
        }
        hexa << "\n"
        hd = Digest::MD5.hexdigest(hexa)

        puts "#{lineno} #{hexa[hexa.size-34..hexa.size-2]} #{chunk.bytes.size}"
=end

	output <<  chunk + NFM
    #output << chunk 
    #output << "  "
    #output.setbyte(output.bytes.size-2, 0xff) # set NFM first byte
    #output.setbyte(output.bytes.size-1, 0xfe) # set NFM second byte
}

begin
    File.open(outfname, "w").write(output)
rescue
    STDERR.puts "Error: attempting to write #{outfname}"
    exit 1
end

puts "#{outfname} muxed OK"
