#!/usr/bin/ruby
require 'pp'

class String
    def ascii!
        self.force_encoding("ascii-8bit")
    end
    def utf8!
        self.force_encoding("utf-8")
    end
end

NSM = "\xFF\xFF".ascii!  # next stream marker
NFM = "\xFF\xFE".ascii!  # next frame marker
NCM = "\xFF\xFD".ascii!  # next chunk marker

COLUMNSIZE=20
streams = []
streamwidths = []
idx = 0
frameno=0
chunkno = 0


fname = ARGV.first
txt = File.open(fname).read.ascii!

begin
	chunk="".ascii!
	cb = txt.bytes[idx]
	nextbyte=0
	
	until (cb == 0xff) 	
		chunk = chunk + (cb.chr)
		#puts "cb #{cb} #{cb.class} - idx #{idx.class} #{idx} - chunk #{chunk.class} \"#{chunk}\""
		idx=idx+1	
		if idx>=txt.bytes.size-1 then
			nextbyte=-1
			break
		end
		cb = txt.bytes[idx]
	end 

	nextbyte = txt.bytes[idx+1] if nextbyte!=-1

	case nextbyte
		when -1 # plain text file, just one simple stream
			streams[0]=chunk.split("\n")
			longest=0 
			streams[0].each {|line| 
				longest = line.bytes.size if line.bytes.size>longest 
			}
			streamwidths[0]=longest 
			break 

		when 0xff # NSM.last 
			#puts "NSM found at #{idx}"
			streams[chunkno]=[] if !streams[chunkno]

			# convert chunk to utf8 encoding before
			# measuring string length
			uchunk = chunk.clone.utf8!

			# discard BOM signature if present (EF BB BF)
			# only for the purpose of calculating column width
			# and displaying text on screen, BOM it's otherwise
			# preserved by muxt and demuxt
			if (uchunk.bytes[0..2] == [0xef, 0xbb, 0xbf])  then
				#the sliced copy of uchunk starts at the first utf-8 printable character
				uchunk=uchunk[1..-1] 
			end

			streams[chunkno][frameno] = uchunk 
			
			if !streamwidths[chunkno] then
				streamwidths[chunkno] = uchunk.size	
			else
				if uchunk.size>streamwidths[chunkno] then
					streamwidths[chunkno]=uchunk.size 
				end
			end	
			chunkno=chunkno+1
			idx=idx+2

		when 0xfe # NFM.last
			#puts "NFM found at #{idx}"
			idx=idx+2
			chunkno=0
			frameno=frameno+1
		else
			throw "Invalid xtxt sequence at index #{idx}"
	end
end until idx>=txt.size-1 

streamlengths=[]
streams.each {|astream| streamlengths << astream.size }
longest_stream = streamlengths.max

=begin
pp streams
pp  streamlengths
puts "longest stream: #{longest_stream} "
=end

# TO-DO: correct this in case we allow for 
# renumbering with command line options
linenochars = longest_stream.to_s.size  
linenochars = 2 if linenochars<2
linenochars = "%0#{linenochars}d"

0.upto(longest_stream-1) {|fno|
	line = ""
	0.upto(streams.size-1) {|streamno|
		# TO-DO: make columnsize a command line option

		# COLUMNSIZE was used to make
		# all columns the same width
		# columnsize=COLUMNSIZE

		# make each column the width of the 
		# longest line for every column
		columnsize = streamwidths[streamno]

		column = streams[streamno][fno] ? streams[streamno][fno] : ""
		column = column.chomp.utf8!		
        column = column
		line << column.ljust(columnsize)[0..columnsize-1]
	}		

	# TO-DO: make line numbering a command line option
	puts "#{linenochars.%(fno+1)} #{line}"
}


=begin

# this is work in progress, preparing muxcat to take
# command line options to make it useful for some
# practical tasks.

opshortfmt = {
        :f => /^\-[a-z]\:[0-9]*\.[0-9]*/, # float
        :i => /^\-[a-z]\:*[0-9]/,         # integer
        :b => /^\-[a-z]/                 # boolean flag
}

oplongfmt = {
        :f => /^\-\-[a-z]\b:[0-9]*\.[0-9]*/, # float
        :i => /^\-\-[a-z]\:*[0-9]/,         # integer
        :b => [ /^\-\-*[a-z]/ ,  /^\-\-no\-*[a-z]/ ] # float
}


opts ={ # option type    long_format   mandatory description
        "h" => { :t=>:b, l: "head"    ,m: false, d: "Treat the first line as a header" },
        "l" => { :t=>:i, l: "line"    ,m: false, d: "Extract specified line"},
        "w" => { :t=>:w, l: "width"   ,m: false, d: "Specify colum width"},
        "n" => { :t=>:b, l: "numbers" ,m: false, d: "Display line numbers"},
        "s" => { :t=>:i, l: "stream"  ,m: false, d: "Display specified stream only"}
}
=end
